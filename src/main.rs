extern crate glaucous;
extern crate fs_extra;

use fs_extra::dir::{copy, CopyOptions};

fn main() {
    std::fs::create_dir_all("firebase/public/amber/").expect("Failed to create firebase/public folder");
    std::fs::create_dir_all("firebase/public/introspection/").expect("Failed to create firebase/public folder");
    std::fs::create_dir_all("firebase/public/pizzatopia/pictures/").expect("Failed to create firebase/public folder");
    std::fs::create_dir_all("firebase/public/pizzatopia/videos/").expect("Failed to create firebase/public folder");

    let mut glauc = glaucous::Glaucous::new();

    // Amber
    /*
    let website_files = vec![
        ("amber/index.html", ""),
        ("amber/2019-09-28.html", "Freezing Cold"),
    ];
    glauc.render_and_publish_isolated_context(&website_files, "/amber/");
    */

    // Introspection
    let website_files = vec![
        ("introspection/index.html", ""),
        ("introspection/manifesto.html", "Personal Manifesto"),
    ];
    glauc.render_and_publish_isolated_context(&website_files, "/introspection/");

    // Pizzatopia
    let website_files = vec![
        ("pizzatopia/index.html", ""),
        ("pizzatopia/2019_11_23.html", "A wild post appeared! (2019-11-23)"),
        ("pizzatopia/2019_11_24.html", "Say no to levitation (2019-11-24)"),
        ("pizzatopia/2020_01_13.html", "Kitty goes meow (2020-01-13)"),
        ("pizzatopia/2020_01_26.html", "Cats are ninjas (2020-01-26)"),
        ("pizzatopia/2020_04_29.html", "Time to trap the cat(s) (2020-04-29)"),
        ("pizzatopia/2020_05_02.html", "Trap the cat vol. 2 - camera strikes back (2020-05-02)"),
    ];
    glauc.render_and_publish_isolated_context(&website_files, "/pizzatopia/");

    // Base
    let website_files = vec![("index.html", "")];
    glauc.render_and_publish_global_index(&website_files, "/");

    // Files to copy to the base
    let to_copy = vec!["style.css"];
    glaucous::copy_static_files(to_copy, "static/", "firebase/public/");

    // Copy the assets to firebase folder
    let mut options = CopyOptions::new(); //Initialize default values for CopyOptions
    options.skip_exist = true;
    copy("assets/pictures", "firebase/public", &options);
    copy("assets/videos", "firebase/public", &options);
}
